#!/bin/bash -e

install -m 644 files/mosimpa.list "${ROOTFS_DIR}/etc/apt/sources.list.d/"
sed -i "s/RELEASE/${RELEASE}/g" "${ROOTFS_DIR}/etc/apt/sources.list.d/mosimpa.list"

on_chroot apt-key add - < files/mosimpa.gpg.key
on_chroot << EOF
apt-get update
apt-get dist-upgrade -y
EOF
