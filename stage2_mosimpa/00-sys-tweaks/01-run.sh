#!/bin/bash -e

install -m 755 files/set_hostname_once	"${ROOTFS_DIR}/etc/init.d/"

if [ "${USE_QEMU}" = "1" ]; then
	echo "enter QEMU mode"
	on_chroot << EOF
systemctl disable set_hostname_once
EOF
	echo "leaving QEMU mode"
else
	on_chroot << EOF
systemctl enable set_hostname_once
EOF
fi

