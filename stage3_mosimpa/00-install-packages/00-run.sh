#!/bin/bash -e

# mosquitto config.
install -m 644 files/mosimpa.conf "${ROOTFS_DIR}/etc/mosquitto/conf.d/"

# Default LXDE pannel settings.
install -v -o 1000 -g 1000 -d "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.config/lxpanel/LXDE-pi/panels"
install -v -o 1000 -g 1000 -m 644 files/panel "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.config/lxpanel/LXDE-pi/panels/"

# Default ABM config.
install -v -o 1000 -g 1000 -d "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.config/MoSimPa"
install -v -o 1000 -g 1000 -m 644 files/abm.conf "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/.config/MoSimPa/"

# Somehow the user is not the right one.
on_chroot << EOF
chown -R ${FIRST_USER_NAME}:${FIRST_USER_NAME} /home/${FIRST_USER_NAME}/.config
EOF
