#!/bin/bash -e

# Activate the default ssl site.
a2ensite default-ssl
a2enmod ssl
# Disactivate the default site.
a2dissite 000-default.conf
# Reload apache2.
systemctl reload apache2

# Let ntp be used as a server and use the system clock if there are no other references.
cat <<EOF >> /etc/ntp.conf

# Let ntp be used as a server.
restrict 0.0.0.0

# Use the system clock if there are no other references. This will take some time.
server 127.127.1.0 local clock
fudge 127.127.1.0 stratum 12
EOF

# Disable screen blanking.
raspi-config nonint do_blanking 1
