#!/bin/bash -e

# Somehow PostgreSql ended up running on port 5433 instead of 5432...

echo "Ensure that PostgreSql uses the right port number"

sed -i 's/port = 543./port = 5432/g' /etc/postgresql/11/main/postgresql.conf
